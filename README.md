# `wordle-guru`

My approach involves attempting to maximise the reduction of possible words with every guess.

We can model a Worlde game at guess `i`: as state: `S_i`. Everytime we make a guess, additional constraints on the correct word are added and the guess increments. Thus: `guess(S_i, g) = S_(i+1)`.

If `L` is the set of all valid Worlde guesses, then `P(L,S_i)` is the set of these guesses that still satisfy the constraints of `S_i`.

Formally, we want to find a guess `g` such that the expected value of `P(L,S_i) - P(L,guess(S_i, g))` is maximised.

We can do this by iterating through each `l` from `L`, and then for each `j` from `P(L,S_i)`, _pretending_ that `j` is the actual word we are trying to guess. We can then "guess" our `l` and see how much `P(L,S_i)` further reduces. Each `j` is equally likely to actually be the correct word and thus expected value can be found by taking the average of all these reductions for a given `l`.

Unfortunately this is prohibitively expensive (at least for my pea-sized brain). So we do a few dirty hacks to speed things up:

  1. Pre-compute the first guess. The initial state of Wordle is always the same so this only has to be computed once. I haven't done this, but read online that it is `C R A N E` - so that is what I went with.
  2. Instead of iterating through all of `l` from `L` and `j` from `P(L,S_i)`, we take a random, smaller sample of each.
  3. Use the `multiprocessing` library to parallelize the analysis of each `l`.
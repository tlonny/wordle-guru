from string import ascii_lowercase


class State:
    def __init__(self):
        self.num_guesses = 0
        self.allowed_characters = [set(ascii_lowercase) for _ in range(5)]
        self.required_characters = set()
        self.guessed_characters = [False for _ in range(5)]

    def clone(self):
        state = State()
        state.num_guesses = self.num_guesses
        state.allowed_characters = [set(x) for x in self.allowed_characters]
        state.gussed_characters = [x for x in self.guessed_characters]
        state.required_characters = set(self.required_characters)
        return state

    def is_valid_word(self, word):
        for ix, c in enumerate(word):
            if c not in self.allowed_characters[ix]:
                return False

        for c in self.required_characters:
            if c not in word:
                return False

        return True

    def set_correct_character(self, ix, char):
        self.allowed_characters[ix] = set(char)
        self.guessed_characters[ix] = True
        self.required_characters.discard(char)

    def set_partial_character(self, ix, char):
        self.allowed_characters[ix].discard(char)
        self.required_characters.add(char)

    def set_missing_character(self, char):
        for ix, s in enumerate(self.allowed_characters):
            if not self.guessed_characters[ix]:
                s.discard(char)

    def guess(self, guess):
        self.num_guesses += 1

        for ix, c in enumerate(guess):
            if c == self.word[ix]:
                self.set_correct_character(ix, c)
            elif c in [
                w for (ix, w) in enumerate(self.word) if not self.guessed_characters[ix]
            ]:
                self.set_partial_character(ix, c)
            else:
                self.set_missing_character(c)
        return guess == self.word

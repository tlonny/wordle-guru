from argparse import ArgumentParser
from functools import cache
from multiprocessing import Pool
from random import sample, choice
from guru.lib.state import State
from guru.lib.log import debug, info, warn


def get_target_sample_size():
    return 500


def get_guess_sample_size():
    return 300


def load_words(path):
    words = list()
    with open(path) as f:
        for word in f:
            word = word.strip()
            if len(word) == 5:
                words.append(word)
    return words


@cache
def get_dictionary():
    return load_words("data/dictionary.txt")


@cache
def get_targets():
    return load_words("data/targets.txt")


def analyze_guess(args):
    guess, state = args
    possible_words = [x for x in get_dictionary() if state.is_valid_word(x)]

    target_sample_size = min(get_target_sample_size(), len(possible_words))
    sample_targets = sample(possible_words, target_sample_size)

    total_words = 0
    for target in sample_targets:
        hypothetical_state = state.clone()
        hypothetical_state.word = target
        hypothetical_state.guess(guess)
        total_words += len(
            [x for x in possible_words if hypothetical_state.is_valid_word(x)]
        )
    average_words = total_words / target_sample_size
    average_reduction = len(possible_words) - average_words
    debug(f"guess: {guess} with average reduction of: {average_reduction:.2f}")
    return (guess, average_reduction)


def select_guess(state: State):
    remaining_words = [x for x in get_dictionary() if state.is_valid_word(x)]
    info(f"currently: {len(remaining_words)} candidate words left")
    if state.num_guesses == 5 or len(remaining_words) == 1:
        if len(remaining_words) > 1:
            warn("potential loss due to multiple remaining words")
        return choice(remaining_words)

    with Pool() as pool:
        guess_sample_size = min(get_guess_sample_size(), len(get_dictionary()))
        guesses = [(x, state) for x in sample(get_dictionary(), guess_sample_size)]
        guesses = pool.map(analyze_guess, guesses)

    best_guess, best_reduction = max(guesses, key=lambda x: x[1])
    info(f"best guess: {best_guess} with average reduction of: {best_reduction:.2f}")
    return best_guess


def get_args():
    parser = ArgumentParser()
    parser.add_argument("target")
    return parser.parse_args()


def play(target):
    state = State()
    state.word = target
    
    info(f"guess: 1 is: crane")
    state.guess("crane")

    for ix in range(2, 7):
        guess = select_guess(state)
        info(f"guess: {ix} is: {guess}")
        if state.guess(guess):
            return True, ix
    return False, None


if __name__ == "__main__":
    args = get_args()

    if args.target not in get_targets():
        raise RuntimeError(f"Target: {args.target} is invalid")

    result, num_guesses = play(args.target)

    if result:
        print(f"[result] {args.target}: found_{num_guesses}", flush = True)
    else:
        print(f"[result] {args.target}: not_found", flush = True)
